"""
Copyright (c) 2016-2020 Keith Sterling http://www.keithsterling.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""
import telegram
from telegram import ParseMode
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from programy.utils.logging.ylogger import YLogger
from programy.clients.polling.client import PollingBotClient
from programy.clients.polling.telegram.config import TelegramConfiguration
from programy.utils.console.console import outputLog
from programy.clients.polling.telegram.renderer import TelegramRenderer

def start(telegram_bot, update):
    if TelegramBotClient.TELEGRAM_CLIENT is None:
        raise Exception("Please initialise Telegram Client first")
    TelegramBotClient.TELEGRAM_CLIENT.start(telegram_bot, update)


def message(telegram_bot, update):
    if TelegramBotClient.TELEGRAM_CLIENT is None:
        raise Exception("Please initialise Telegram Client first")
    TelegramBotClient.TELEGRAM_CLIENT.message(telegram_bot, update)


def unknown(telegram_bot, update):
    if TelegramBotClient.TELEGRAM_CLIENT is None:
        raise Exception("Please initialise Telegram Client first")
    TelegramBotClient.TELEGRAM_CLIENT.unknown(telegram_bot, update)


class TelegramBotClient(PollingBotClient):

    TELEGRAM_CLIENT = None

    def __init__(self, argument_parser=None):
        self._updater = None
        self._telegram_token = None
        PollingBotClient.__init__(self, "telegram", argument_parser)

    def get_client_configuration(self):
        return TelegramConfiguration()

    def get_license_keys(self):
        self._telegram_token = self.license_keys.get_key("TELEGRAM_TOKEN")

    def create_updater(self, telegram_token):
        self._updater = Updater(token=telegram_token)

    def callback_query_handler(self, telegram_bot, update):
        try:
            client_context = self.create_client_context(update.callback_query.message.chat.id)
            response = self.ask_question(update.callback_query.message.chat.id, update.callback_query.data)
            extracted_data = TelegramRenderer.render(TelegramRenderer(),client_context,response)
            if len(extracted_data) !=0:
                self.send_rendered_message(extracted_data,telegram_bot, update.callback_query.message.chat.id)
            else:
                telegram_bot.send_message(chat_id=update.callback_query.message.chat.id, text=response, parse_mode=ParseMode.HTML)

        except Exception as e:
            telegram_bot.send_message(chat_id=update.callback_query.message.chat.id, text='something went wrong', parse_mode=ParseMode.HTML)
            YLogger.exception(self, "Failed to handle message", e)

    def register_handlers(self):
        start_handler = CommandHandler('start', start)
        message_handler = MessageHandler(Filters.text, message)
        unknown_handler = MessageHandler(Filters.command, unknown)
        callback_handler = telegram.ext.CallbackQueryHandler(self.callback_query_handler)

        self._updater.dispatcher.add_handler(start_handler)
        self._updater.dispatcher.add_handler(message_handler)
        self._updater.dispatcher.add_handler(callback_handler)
        # Add unknown last
        self._updater.dispatcher.add_handler(unknown_handler)

    def get_initial_question(self, update):
        client_context = self.create_client_context(update.message.chat_id)
        initial_question = client_context.bot.get_initial_question(client_context)
        processed_question = client_context.bot.post_process_response(client_context,
                                                                      initial_question,
                                                                      srai=False)
        return processed_question

    def ask_question(self, userid, question):
        self._questions += 1
        client_context = self.create_client_context(userid)
        response = client_context.bot.ask_question(client_context, question.replace('\n',' '), responselogger=self)
        return response

    def start(self, telegram_bot, update):
        try:
            initial_question = self.get_initial_question(update)
            if initial_question:
                telegram_bot.send_message(chat_id=update.message.chat_id, text=initial_question, parse_mode=ParseMode.HTML)
            else:
                YLogger.error(self, "Not initial question to return in start()")

        except Exception as e:
            YLogger.exception(self, "Failed to start", e)

    def message(self, telegram_bot, update):
        try:
            client_context = self.create_client_context(update.message.chat_id)
            response = self.ask_question(update.message.chat_id, update.message.text)
            # print(response)
            extracted_data = TelegramRenderer.render(TelegramRenderer(),client_context,response)
            if len(extracted_data) !=0:
                self.send_rendered_message(extracted_data,telegram_bot, update.message.chat_id)
            else:
                telegram_bot.send_message(chat_id=update.message.chat_id, text=response, parse_mode=ParseMode.HTML)
        except Exception as e:
            YLogger.exception(self, "Failed to handle message", e)

    def send_rendered_message(self, extracted_data, telegram_bot, chat_id):
        if extracted_data is not None :
            message_type = ''
            custom_keyboard = []
            print(extracted_data)
            for msg in extracted_data:
                buttons = []
                if msg['type'] == "button":
                    if msg['url'] is not None:
                        buttons.append(
                            [telegram.InlineKeyboardButton(text = msg['text'].replace('\n',''), url = msg['url'].replace('\n',''))]
                        )
                    elif msg['postback'] is not None:
                        buttons.append(
                            [telegram.InlineKeyboardButton(text = msg['text'].replace('\n',''), callback_data = msg['postback'].replace('\n',''))]
                        )
                if len(buttons) > 0:
                        keyboard = telegram.InlineKeyboardMarkup(buttons)
                        telegram_bot.send_message(chat_id=chat_id, text='Please select the below Options.',reply_markup = keyboard)
                elif msg['type']== "link":
                    text = "<a href='"+msg['url'].replace('\n','')+"'>"+msg['text'].replace('\n','')+"</a>"
                    telegram_bot.send_message(chat_id=chat_id,text=text, parse_mode=ParseMode.HTML)
                elif msg['type'] == "image":
                    telegram_bot.send_photo(chat_id=chat_id, photo=str(msg['url'].replace('\n','')))
                elif msg['type'] == "video":
                    ""
                elif msg['type'] == "card":
                    buttons = []
                    if msg['image']:
                        telegram_bot.send_photo(chat_id=chat_id, photo=str(msg['image'].replace('\n','')), caption=msg['title'].replace('\n',''))
                    for btn in msg['buttons']:
                        if btn['url'] is not None:
                            buttons.append(
                                [telegram.InlineKeyboardButton(text = btn['text'].replace('\n',''), url = btn['url'].replace('\n',''))]
                            )
                        elif btn['postback'] is not None:
                            buttons.append(
                                [telegram.InlineKeyboardButton(text = btn['text'].replace('\n',''), callback_data = btn['postback'].replace('\n',''))]
                            )
                    if len(buttons) > 0:
                        keyboard = telegram.InlineKeyboardMarkup(buttons)
                        if msg['title']:
                            telegram_bot.send_message(chat_id=chat_id, text=msg['title'],reply_markup = keyboard)
                        else:
                            telegram_bot.send_message(chat_id=chat_id, text='Please select the below Options.',reply_markup = keyboard)
                elif msg['type'] == "carousel":
                    for card in msg['cards']:
                        buttons = []
                        if msg['image']:
                            telegram_bot.send_photo(chat_id=chat_id, photo=str(msg['image'].replace('\n','')), caption=msg['title'].replace('\n',''))
                        for btn in card['buttons']:
                            if btn['url'] is not None:
                                buttons.append(
                                    [telegram.InlineKeyboardButton(text = btn['text'].replace('\n',''), url = btn['url'].replace('\n',''))]
                                )
                            elif btn['postback'] is not None:
                                buttons.append(
                                    [telegram.InlineKeyboardButton(text = btn['text'].replace('\n',''), callback_data = btn['postback'].replace('\n',''))]
                                )
                        if len(buttons) > 0:
                            keyboard = telegram.InlineKeyboardMarkup(buttons)
                            if msg['title']:
                                telegram_bot.send_message(chat_id=chat_id, text=msg['title'],reply_markup = keyboard)
                            else:
                                telegram_bot.send_message(chat_id=chat_id, text='Please select the below Options.',reply_markup = keyboard)
                elif msg['type'] == "reply":
                    custom_keyboard.append(str(msg['text'].replace('\n','')))
                elif msg['type'] == "text":
                    telegram_bot.send_message(chat_id=chat_id, text=msg['text'].replace('\n',''), parse_mode=ParseMode.HTML)
            if len(custom_keyboard) > 0:
                text = 'Please Select the below options'
                reply_markup = telegram.ReplyKeyboardMarkup(
                    [custom_keyboard], one_time_keyboard=True,resize_keyboard=True, selective=True)
                telegram_bot.send_message(chat_id=chat_id,  text=text,
                    reply_markup=reply_markup)
        else:
            telegram_bot.send_message(chat_id=chat_id, text="hello", parse_mode=ParseMode.HTML)
            YLogger.error(self, "Not response to return in message()")

    def get_unknown_response(self, userid):
        return self.ask_question(userid, self.configuration.client_configuration.unknown_command_srai)

    def get_unknown_command(self, userid):
        if self.configuration.client_configuration.unknown_command_srai is None:
            unknown_response = self.configuration.client_configuration.unknown_command
        else:
            unknown_response = self.get_unknown_response(userid)
            if unknown_response is None or unknown_response == "":
                unknown_response = self.configuration.client_configuration.unknown_command
        return unknown_response

    def unknown(self, telegram_bot, update):
        try:
            unknown_response = self.get_unknown_command(update.message.chat_id)
            if unknown_response:
                telegram_bot.send_message(chat_id=update.message.chat_id, text=unknown_response)
                YLogger.error(self, "No response to return in unknown()")

        except Exception as e:
            YLogger.exception(self, "Failed to handle unknown", e)

    def display_connected_message(self):
        outputLog(self, "Telegram Bot connected and running...")

    def connect(self):
        if self._telegram_token is not None:
            self.create_updater(self._telegram_token)
            self.register_handlers()
            return True

        outputLog(self, "No telegram token defined, unable to connect")
        return False

    def poll_and_answer(self):

        running = True
        try:
            self._updater.start_polling()
            # Without this the system goes into 100% CPU utilisation
            self._updater.idle()

        except KeyboardInterrupt:
            outputLog(self, "Telegram client stopping....")
            running = False
            self._updater.stop()

        except Exception as excep:
            YLogger.exception(self, "Failed to poll and answer", excep)

        return running


if __name__ == '__main__':

    outputLog(None, "Initiating Telegram Client...")

    TelegramBotClient.TELEGRAM_CLIENT = TelegramBotClient()
    TelegramBotClient.TELEGRAM_CLIENT.run()

