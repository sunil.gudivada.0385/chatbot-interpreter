from programy.utils.logging.ylogger import YLogger
from programy.extensions.base import Extension
from lxml import etree

class CustomExtension(Extension):

    # execute() is the interface that is called from the <extension> tag in the AIML
    def execute(self, client_context, data):
        YLogger.debug(client_context, "Survey Storage - Storing data [%s]", data)

        # Data is bar delimited, so you could write to a file, add to a database, or send to another REST service
        print(data)
        root=etree.Element('li')
        root.text="custom extension"

        return root
